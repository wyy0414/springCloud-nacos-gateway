package com.project.common.core.utils.wx.pojo.menu;

import java.io.Serializable;

public class Button implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
