package com.project.common.core.utils.exception;

/**
 * 响应编码
 */
public enum RESP_CODE_ENUM {
    /* ******************** 业务自定义异常 ********************* */
    RATE_LIMITER("1001", "API网关限流"),
    RSA_NOT_EXIST("1002", "RSA非对称加密公钥不存在"),
    PWD_ERROR("1003", "密码错误"),
    TOKEN_IS_NOT_EXIST("1004", "TOKEN已失效"),
    REQUIRED_IDENTIFY_NOT_EXIST("1005", "请求标识对象不存在"),
    ILLEGAL_PARAMETER("1006", "非法参数"),
    MISS_PARAMETER("1007", "缺少必须的参数"),
    ACCT_NOT_EXIST("1008", "账号不存在"),
    ACCT_OR_ACCOPASS_ERROR("1009", "账号或密码错误"),
    ACCT_HAS_FROZEN("1010", "账号已冻结"),
    NOT_LOGIN_ERROR("1011", "用户未登录"),
    CAPTCHA_CODE_INVALID("1012", "验证码失效"),
    CAPTCHA_CODE_ERROR("1013", "验证码错误"),
    NO_DATA("1014", "数据为空"),
    CAPTCHA_EXIST("1015", "验证码已发送，请稍等"),
    SERVICE_ERROR("1016", "服务报错"),
    ERROR_DATA_STATUS("1017", "数据状态不正确"),
    MICRO_SERVICE_ERROR("1018", "微服务调用报错"),
    SAVE_CHAPTER_ERROR("1019", "保存课程章节出错");

    // 错误编码
    private String code;

    // 错误信息
    private String msg;

    // 相应编码有参构造函数
    RESP_CODE_ENUM(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 获取编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取编码信息
     */
    public String getMsg() {
        return msg;
    }

    /**
     * 设置编码信息
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
