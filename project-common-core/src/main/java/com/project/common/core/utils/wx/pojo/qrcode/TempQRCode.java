package com.project.common.core.utils.wx.pojo.qrcode;

/**
 * 临时二维码
 */
public class TempQRCode extends QRCode {

    public static final String ACTION_NAME_FINAL = "QR_SCENE";//临时二维码固定值

    public static final int EXPIRE_SECONDS_FINAL = 604800;//临时二维码有效期

    private int expire_seconds = EXPIRE_SECONDS_FINAL;

    public TempQRCode() {
        this.action_name = ACTION_NAME_FINAL;
    }

    public TempQRCode(ActionInfo actionInfo) {
        super(actionInfo);
        this.action_name = ACTION_NAME_FINAL;
    }

    public TempQRCode(Scene scene) {
        super(scene);
        this.action_name = ACTION_NAME_FINAL;
    }

    public int getExpire_seconds() {
        return expire_seconds;
    }

    public void setExpire_seconds(int expire_seconds) {
        this.expire_seconds = expire_seconds;
    }
}
