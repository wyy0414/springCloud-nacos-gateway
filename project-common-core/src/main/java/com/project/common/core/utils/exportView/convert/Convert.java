package com.project.common.core.utils.exportView.convert;

/**
 * 类型转化接口
 */
public interface Convert {
    String convert(String value);
}
