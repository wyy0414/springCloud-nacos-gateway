package com.project.common.core.utils.wx.pojo.qrcode;

/**
 * id的场景值
 */
public class IdScene extends Scene {

    private long scene_id;

    public IdScene(long scene_id) {
        this.scene_id = scene_id;
    }

    public long getScene_id() {
        return scene_id;
    }

    public void setScene_id(long scene_id) {
        this.scene_id = scene_id;
    }
}
