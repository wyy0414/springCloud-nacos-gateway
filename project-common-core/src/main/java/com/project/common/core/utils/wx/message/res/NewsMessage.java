package com.project.common.core.utils.wx.message.res;

import java.util.List;

public class NewsMessage extends BaseMessage {

    //图文消息个数，限制在10个以内
    private int ArticleCount;

    //多条图文消息，默认第一个为大图
    private List<Article> Articles;

    public int getArticleCount() {
        return ArticleCount;
    }

    public void setArticleCount(int articleCount) {
        ArticleCount = articleCount;
    }

    public List<Article> getArticles() {
        return Articles;
    }

    public void setArticles(List<Article> articles) {
        Articles = articles;
    }

}
