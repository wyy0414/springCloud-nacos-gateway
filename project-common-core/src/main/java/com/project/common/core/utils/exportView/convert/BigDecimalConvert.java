package com.project.common.core.utils.exportView.convert;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * BigDecimal 转化类
 */
public class BigDecimalConvert implements Convert {

    private int scale = 2;

    public BigDecimalConvert() {

    }

    public BigDecimalConvert(int scale) {
        this.scale = scale;
    }

    @Override
    public String convert(String value) {

        if (StringUtils.isBlank(value)) {
            return "";
        }

        try {
            BigDecimal b = new BigDecimal(value);
            return b.setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
