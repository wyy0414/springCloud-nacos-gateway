package com.project.common.core.utils.wx.pojo.qrcode;

/**
 * id场景永久二维码
 */
public class LimitIDQRCode extends QRCode {

    public static final String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";//Id场景的action_name

    public LimitIDQRCode() {
        this.action_name = QR_LIMIT_SCENE;
    }

    public LimitIDQRCode(ActionInfo actionInfo) {
        super(actionInfo);
        this.action_name = QR_LIMIT_SCENE;
    }

    public LimitIDQRCode(Scene scene) {
        super(scene);
        this.action_name = QR_LIMIT_SCENE;
    }
}
