package com.project.common.core.utils.wx.message.req;

/**
 * 请求文本消息
 */
public class TextMessage extends BaseMessage {

    //消息内容
    private String Content;

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

}
