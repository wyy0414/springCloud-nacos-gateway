package com.project.common.core.utils.wx.message.req;

import java.io.Serializable;

/**
 * 请求消息基类
 */
public class BaseMessage implements Serializable {

    private static final long serialVersionUID = 7678251814240402448L;
    //开发者微信号
    private String ToUserName;
    //发送者账号
    private String FromUserName;
    //消息创建时间
    private long CreateTime;
    //消息类型（text/image/location/link）
    private String MsgType;
    //消息
    private long MsgId;

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(long createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }

    public long getMsgId() {
        return MsgId;
    }

    public void setMsgId(long msgId) {
        MsgId = msgId;
    }

}
