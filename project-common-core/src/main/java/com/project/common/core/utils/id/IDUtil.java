package com.project.common.core.utils.id;


import com.project.common.core.config.prop.PropAttributes;
import com.project.common.core.config.prop.PropConfig;

/**
 * id生成工具类
 */
public class IDUtil {

    private static IDWorker instance = null;

    private IDUtil() {

    }

    public static IDWorker getInstance() {
        if (instance == null) {
            synchronized (IDUtil.class) {
                if (instance == null) {
                    instance = new IDWorker(Integer.valueOf(PropConfig.getProperty(PropAttributes.SYSTEM_SERVICE_WORKERID)),
                            Integer.valueOf(PropConfig.getProperty(PropAttributes.SYSTEM_SERVICE_DATAID)));
                }
            }
        }
        return instance;
    }

    public static long getId() {
        return getInstance().nextId();
    }
}
