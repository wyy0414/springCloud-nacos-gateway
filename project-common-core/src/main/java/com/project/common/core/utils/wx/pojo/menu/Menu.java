package com.project.common.core.utils.wx.pojo.menu;

import java.io.Serializable;
import java.util.List;

public class Menu implements Serializable {

    private List<Button> button;

    public List<Button> getButton() {
        return button;
    }

    public void setButton(List<Button> button) {
        this.button = button;
    }
}
