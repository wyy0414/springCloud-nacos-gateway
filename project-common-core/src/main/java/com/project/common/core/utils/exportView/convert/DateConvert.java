package com.project.common.core.utils.exportView.convert;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * 日期类型转化
 */
public class DateConvert implements Convert {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.US);

    private SimpleDateFormat outSDF = null;

    public DateConvert() {
        this(null);
    }

    public DateConvert(String pattern) {
        if (StringUtils.isBlank(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        outSDF = new SimpleDateFormat(pattern);
    }

    @Override
    public String convert(String value) {
        try {
            if (StringUtils.isNotBlank(value)) {
                return outSDF.format(SDF.parse(value));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
