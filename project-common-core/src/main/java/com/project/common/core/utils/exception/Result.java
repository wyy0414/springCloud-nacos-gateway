package com.project.common.core.utils.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author wyy
 * @date 2019-10-11 20:51
 */
@ApiModel("返回结果")
public class Result<T> {
    // 结果标识
    @ApiModelProperty(value = "结果标识",example = "success")
    private String result;

    // 错误编码
    @ApiModelProperty(value = "错误编码",example = "1001")
    private String errorCode;

    // 错误信息
    @ApiModelProperty(value = "错误信息",example = "操作成功")
    public String errorMsg;

    // 封装数据的参数名称
    @ApiModelProperty(value = "返回结果")
    private T data;

    /**
     * 结果参数枚举
     */
    public enum RESULT_FLG {
        SUCCESS("success"),
        FAIL("fail");

        /**
         * 值变量
         */
        private String value;

        /**
         * 含有结果值的构造函数
         *
         * @param value
         */
        private RESULT_FLG(String value) {
            this.value = value;
        }

        /**
         * 获取值
         *
         * @return
         */
        public String getValue() {
            return this.value;
        }
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
