package com.project.common.core.config.prop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wyy
 * @date 2019-07-25 01:39
 */
@Component
public class PropertiesListener implements ApplicationListener<ApplicationStartedEvent> {

    /**
     * 属性文件集合
     */
    @Value("${system.properties.files}")
    private List<String> propFileNameList;

    /**
     * 事件监听
     *
     * @param applicationStartedEvent
     */
    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        PropConfig.loadAllProperties(propFileNameList);
    }
}
