package com.project.common.core.utils.redis;

/**
 * redis 常量
 */
public class RedisConsts {
    /****************************** 后台管理端缓存设置开始 ******************************/
    // 系统setting
    public static final String SYSTEM_SETTING = "system:setting:setting";
    // 接口Token参数设置
    public final static String ADMIN_ACCTNAME = "admin:acctName:";
    public final static int ADMIN_ACCTNAME_EXPIRE = 60 * 60 * 24 * 30;
    /****************************** 后台管理端缓存设置结束 ******************************/

    /* ************************* shiro 设置开始 *********************************/
    // shiroSession的redis-key
    public static final String ADMIN_SHIRO_SESSION_KEY = "admin:shiro_redis_session:";
    // shiroSession的过期时间，单位：秒，此值须大于spring-cache.xml中的全局session有效期
    public static final int ADMIN_SHIRO_SESSION_EXPIRE = 3600;
    // shiroRealm的redis-key
    public static final String ADMIN_SHIRO_REALM_KEY = "admin:shiro_redis_realm:";
    // shiroRealm的过期时间，单位：秒，设置与SHIRO_SESSION_EXPIRE相等
    public static final int ADMIN_SHIRO_REALM_EXPIRE = 3600;
    /* ************************* shiro 设置结束 *********************************/

    /****************************** 手机验证码设置开始 ******************************/
    // 用户手机号验证码的发送次数key
    public static final String MEMBER_MOBILE_CODE = "member:mobileCode:";
    // 用户限制手机号验证码的发送次数时间
    public static final int MEMBER_MOBILE_CODE_EXPIRE = 60 * 2;
    /****************************** 手机验证码设置结束 ******************************/

    /************************************  小程序设置开始 ************************************/
    public static final String WX_MINI_ACCESS_TOKEN = "wx:mini:access:token";
    public static final String WX_MINI_MEDIA_ONE = "wx:mini:media:one";
    public static final String WX_MINI_MEDIA_TOW = "wx:mini:media:tow";
    public static final String WX_MINI_MEDIA_TZ = "wx:mini:media:tz";
    public static final String WX_MINI_MEDIA_XC = "wx:mini:media:xc";
    /************************************  小程序设置结束 ************************************/

    /****************************** 系统微信公众平台设置开始 ******************************/
    public static final String WX_PUBLIC_ACCESSTOKEN = "wx:public:accesstoken";
    public static final String WX_PUBLIC_ACCESSTOKENLOCK = "wx:public:accesstokenLock";
    public static final String WX_PUBLIC_JSTICKET = "wx:public:jsTicket";
    public static final String WX_PUBLIC_JSTICKETLOCK = "wx:public:jsTicketLock";
    /****************************** 系统微信公众平台设置结束 ******************************/

    /****************************** API TOKEN设置开始 ******************************/
    public final static String API_TOKEN_KEY = "api:token:";
    public final static int API_TOKEN_EXPIRE = 30 * 24 * 60 * 60;
    /****************************** API TOKEN设置结束 ******************************/

    /****************************** CRM SC轮次设置开始 ******************************/
    public final static String CRM_SC_ALLOT_LIST = "crm:sc:allot:list";
    /****************************** CRM SC轮次设置结束 ******************************/

    /****************************** 腾讯开发者设置开始 ******************************/
    public final static String TX_AD_ACCESS_TOKEN = "tx:ad:access:token";
    /****************************** 腾讯开发者设置结束 ******************************/

    /****************************** Agora设置开始 ******************************/
    public final static String AGORA_LIVE_RTC_TOKEN = "agora:live:rtc:token";
    public final static String AGORA_YB_RTC_TOKEN = "agora:yb:rtc:token";

    public final static String AGORA_LIVE_RTM_TOKEN = "agora:live:rtm:token";
    public final static String AGORA_YB_RTM_TOKEN = "agora:yb:rtm:token";
    public final static long AGORA_TOKEN_EXPIRE = 60 * 60 * 23;
    /****************************** Agora设置结束 ******************************/

    /****************************** IM设置开始 ******************************/
    public final static String IM_USER_ONLINE_QUEUE = "IM:USER:LOGIN";
    public final static long IM_USER_ONLINE_EXPIRE = 30 * 60;

    public final static String IM_USER_MATCH_QUEUE = "IM:USER:MATCH:QUEUE";
    public final static long IM_USER_MATCH_QUEUE_EXPIRE = 10 * 60 ;
    /****************************** IM设置结束 ******************************/

}
