package com.project.common.core.utils.wx.message.req;

/**
 * 请求语音消息
 */
public class VoiceMessage extends BaseMessage {

    //id
    private String MediaId;
    //格式
    private String Format;

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String format) {
        Format = format;
    }

}
