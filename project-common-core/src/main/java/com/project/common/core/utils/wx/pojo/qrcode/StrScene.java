package com.project.common.core.utils.wx.pojo.qrcode;

/**
 * 字符串场景值
 */
public class StrScene extends Scene {

    private String scene_str;

    public StrScene(String scene_str) {
        this.scene_str = scene_str;
    }

    public String getScene_str() {
        return scene_str;
    }

    public void setScene_str(String scene_str) {
        this.scene_str = scene_str;
    }
}
