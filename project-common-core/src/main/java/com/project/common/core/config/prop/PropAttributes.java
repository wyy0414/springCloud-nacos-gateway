package com.project.common.core.config.prop;

import org.springframework.stereotype.Component;

/**
 * 配置文件属性名称定义
 */
@Component
public class PropAttributes {
    /****************************** ID中心 START ******************************/
    public static final String SYSTEM_SERVICE_DATAID = "system.service.dataID";
    public static final String SYSTEM_SERVICE_WORKERID = "system.service.workerID";
    /****************************** ID中心 END ******************************/

    /****************************** NFS配置 START ******************************/
    public static final String NFS__SERVICE_FILE_SHARE_PATH = "nfs.service.file.share.path";
    public static final String NFS__SERVICE_FILE_TEMP_PATH = "nfs.service.file.temp.path";
    public static final String NFS_SERVICE_FILE_SERVER = "nfs.service.file.server";
    /****************************** NFS配置 END ******************************/

    /****************************** 微信配置 START ******************************/
    public static final String THIRDPARTY_WX_APPID = "thirdParty.weChat.appId";
    public static final String THIRDPARTY_WX_APPSECRET = "thirdParty.weChat.appSecret";
    public static final String THIRDPARTY_WX_TOKEN = "thirdParty.weChat.token";
    public static final String THIRDPARTY_WX_MCHID = "thirdParty.weChat.mchId";
    public static final String THIRDPARTY_WX_KEY = "thirdParty.weChat.key";
    public static final String THIRDPARTY_WX_CERTDIR = "thirdParty.weChat.certDir";
    public static final String THIRDPARTY_WX_DOMAIN = "thirdParty.weChat.domain";
    public static final String THIRDPARTY_WX_SCAN_DOMAIN = "thirdParty.weChat.scan.domain";
    public static final String THIRDPARTY_WX_H5_DOMAIN = "thirdParty.weChat.h5.domain";
    public static final String THIRDPARTY_WX_H5_WAP_NAME = "thirdParty.weChat.h5.wap.name";
    public static final String THIRDPARTY_WX_XFER_CUSTSERV = "thirdParty.weChat.customerService";
    public static final String THIRDPARTY_WECHAT_SEND_CUSTOM_MESSAGE = "thirdParty.weChat.send.custom.message";
    /****************************** 微信配置 END ******************************/

    /****************************** 微信开放平台服务配置 START ******************************/
    public static final String THIRDPARTY_WX_OPEN_LOGIN_APPID = "thirdParty.weChat.open.login.appId";
    public static final String THIRDPARTY_WX_OPEN_LOGIN_APPSECRET = "thirdParty.weChat.open.login.appSecret";
    public static final String THIRDPARTY_WX_OPEN_LOGIN_DOMAIN = "thirdParty.weChat.open.login.domain";
    public static final String THIRDPARTY_WX_OPEN_LOGIN_HTTPS_DOMAIN = "thirdParty.weChat.open.login.https.domain";
    public static final String THIRDPARTY_WX_OPEN_LOGIN_QRCODE_CSS = "thirdParty.weChat.open.login.qrCode.css";
    /****************************** 微信开放平台服务配置 END ******************************/

    /****************************** 微信小程序端 START ******************************/
    public static final String THIRDPARTY_WX_MINI_APPID = "thirdParty.weChat.mini.appId";
    public static final String THIRDPARTY_WX_MINI_APPSECRET = "thirdParty.weChat.mini.appSecret";
    public static final String THIRDPARTY_WX_MINI_KEY = "thirdParty.weChat.mini.Key";
    public static final String THIRDPARTY_WX_MINI_MCHID = "thirdParty.weChat.mini.mchId";
    public static final String THIRDPARTY_WX_MINI_DOMAIN = "thirdParty.weChat.mini.domain";
    /****************************** 微信小程序端 START ******************************/

    /****************************** 阿里大于短信服务配置 START ******************************/
    public static final String THIRDPARTY_SMS_SERVERURL = "thirdParty.sms.serverUrl";
    public static final String THIRDPARTY_SMS_APPKEY = "thirdParty.sms.appKey";
    public static final String THIRDPARTY_SMS_APPSECRET = "thirdParty.sms.appSecret";
    public static final String THIRDPARTY_SMS_SIGNNATURE = "thirdParty.sms.signature";
    public static final String THIRDPARTY_SMS_TEMPLATE_GENERAL = "thirdParty.sms.template.general";
    /****************************** 阿里大于短信服务配置 END ******************************/

}
