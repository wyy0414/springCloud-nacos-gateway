package com.project.common.core.utils.wx.pojo.qrcode;

/**
 * 二维码前缀
 */
public enum QrCodeEnum {

    //加油员营销二维码标识
    stationMemberSpread(1);

    int value;

    QrCodeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
