package com.project.common.core.utils.file;

/**
 * @author wyy
 * @date 2019-08-21 23:50
 */
public class FilePathConsts {
    // 媒体文件
    public static final String FILE_MEDIA_PATH = "/upload/media";

    // 二维码图片路径
    public static final String FILE_QR_PATH = "/upload/qr";

    // 课程类目文件路径
    public static final String COURSE_CATEGORY_PATH = "/course/category";
}
