package com.project.common.core.utils;

/**
 * 公共参数
 */
public final class CommonAttributes {

	/** 日期格式配比 */
	public static final String[] DATE_PATTERNS = new String[] { "HH:mm", "yyyy", "yyyy-MM", "yyyyMM", "yyyy/MM", "yyyy-MM-dd", "yyyyMMdd", "yyyy/MM/dd","yyyy-MM-dd HH:mm", "yyyyMMddHHmm", "yyyy/MM/dd HH:mm", "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", "yyyy/MM/dd HH:mm:ss" };

	/** xml文件路径 */
	public static final String XML_PATH = "/app_config.xml";

	/** properties文件路径 */
	public static final String PROPERTIES_PATH = "/app_config.properties";

	/**
	 * 不可实例化
	 */
	private CommonAttributes() {
	}

}
