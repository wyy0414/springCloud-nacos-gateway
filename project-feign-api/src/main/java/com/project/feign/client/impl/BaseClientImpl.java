package com.project.feign.client.impl;

import com.project.common.core.utils.exception.Result;

/**
 * 基础Feign客户端
 *
 * @Author: wyy
 * @Date: 2020-04-11 17:24
 */
public class BaseClientImpl {
    /**
     * 添加数据到结果对象中
     *
     * @param data 封装接口集合参数
     * @return
     */
    public Result getResult(Object data) {
        Result result = new Result();
        result.setResult(Result.RESULT_FLG.SUCCESS.getValue());
        result.setData(data);
        return result;
    }
}
