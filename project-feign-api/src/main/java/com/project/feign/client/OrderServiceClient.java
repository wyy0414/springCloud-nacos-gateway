package com.project.feign.client;

import com.project.common.core.utils.exception.Result;
import com.project.feign.client.impl.OrderServiceClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 小程序服务调用客户端
 *
 * @Author: wyy
 * @Date: 2020-04-24 15:06
 */
@FeignClient(name = "order-service", fallback = OrderServiceClientImpl.class)
public interface OrderServiceClient {

    @GetMapping(value = "/order/auth/demo/test")
    Result<String> test(@RequestParam("test") String test);
}
