package com.project.feign.client;

import com.project.common.core.utils.exception.Result;
import com.project.feign.client.impl.SystemServiceClientImpl;
import com.project.feign.vo.UserInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 系统服务 feign客户fallback业务处理
 *
 * @Author: wyy
 * @Date: 2020-04-11 17:19
 */
@FeignClient(name = "system-service", fallback = SystemServiceClientImpl.class)
public interface SystemServiceClient {
    /**
     * 根据用户ID查询用户信息
     *
     * @param userId 用户ID
     * @return
     */
    @GetMapping(value = "/system/auth/userInfo/getByUserId")
    Result<UserInfoVO> getByUserId(@RequestParam("userId") String userId);
}
