package com.project.feign.client.impl;

import com.project.common.core.utils.exception.CustomException;
import com.project.common.core.utils.exception.RESP_CODE_ENUM;
import com.project.common.core.utils.exception.Result;
import com.project.feign.client.SystemServiceClient;
import com.project.feign.vo.UserInfoVO;
import org.springframework.stereotype.Component;

/**
 * CRM用户feign客户fallback业务处理
 *
 * @Author: wyy
 * @Date: 2020-04-11 17:24
 */
@Component
public class SystemServiceClientImpl extends BaseClientImpl implements SystemServiceClient {

    /**
     * 根据用户ID查询用户信息
     *
     * @param userId 用户ID
     * @return
     */
    @Override
    public Result<UserInfoVO> getByUserId(String userId) {
        throw new CustomException(RESP_CODE_ENUM.MICRO_SERVICE_ERROR);
    }
}
