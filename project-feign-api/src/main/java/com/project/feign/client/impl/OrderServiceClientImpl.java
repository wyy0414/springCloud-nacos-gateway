package com.project.feign.client.impl;

import com.project.common.core.utils.exception.CustomException;
import com.project.common.core.utils.exception.RESP_CODE_ENUM;
import com.project.common.core.utils.exception.Result;
import com.project.feign.client.OrderServiceClient;
import org.springframework.stereotype.Component;

/**
 * 小程序服务调用客户端
 *
 * @Author: wyy
 * @Date: 2020-04-24 15:08
 */
@Component
public class OrderServiceClientImpl extends BaseClientImpl implements OrderServiceClient {

    @Override
    public Result<String> test(String test) {
        throw new CustomException(RESP_CODE_ENUM.MICRO_SERVICE_ERROR);
    }
}
