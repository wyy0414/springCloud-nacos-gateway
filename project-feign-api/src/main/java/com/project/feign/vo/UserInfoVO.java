/*
 * UserInfo.java
 * Copyright(C) 2020-2099 WYY
 * All rights reserved.
 * -----------------------------------------------
 * 2020-04-09 Created
 */
package com.project.feign.vo;

import java.util.Date;

/**
 * 用户VO类
 *
 * @version 1.0 2020-04-09
 */
public class UserInfoVO {

    /**
     * 用户id
     */
    private String userid;

    /**
     * uniondId标识
     */
    private String unionid;

    /**
     * 英语学院公众号openId
     */
    private String openid;

    /**
     * 活动id
     */
    private String activeid;

    /**
     * 邀请人用户ID
     */
    private String inviteuserid;

    /**
     * 来源渠道
     */
    private String channel;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 0.无性别  1.男   2.女
     */
    private String sex;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 头像
     */
    private String portrait;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 关注场景描述
     */
    private String subscribescene;

    /**
     * 0.未关注(网页授权)1.关注2.取关
     */
    private String isfollow;

    /**
     * 关注时间
     */
    private Date followtime;

    /**
     * 取关时间
     */
    private Date unfollowtime;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 学院小程序openID
     */
    private String xcxopenid;

    /**
     * 影响力
     */
    private Integer effectnum;

    /**
     * 邀请点评次数
     */
    private Integer invitecommentnum;

    /**
     * 微课服务号年卡学习天数
     */
    private Integer wkcoursenum;

    /**
     * 微课学习时长
     */
    private Integer wkstudyduration;

    /**
     * 用户等级
     */
    private Integer userlevel;

    /**
     * 小程序用户新增时间
     */
    private Date xcxcreatetime;

    /**
     * 用户目标内容
     */
    private Integer targetnum;

    /**
     * 是否弹出过福利0未弹出1弹出
     */
    private Integer paritycheck;

    /**
     * 用户目标（可修改）
     */
    private String targetMessage;

    /**
     * 用户测试的等级
     */
    private Boolean testLevel;

    /**
     * 用户晋级课升级的等级
     */
    private Boolean upgradeLevel;

    /**
     * 是否获取过用户信息（授权）0未授权 1授权
     */
    private Boolean userAuth;

    /**
     * 是否获取过手机号（授权）0未授权 1授权
     */
    private Boolean phoneAuth;

    /**
     * 助力用户
     */
    private String helpuserid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid == null ? null : unionid.trim();
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getActiveid() {
        return activeid;
    }

    public void setActiveid(String activeid) {
        this.activeid = activeid == null ? null : activeid.trim();
    }

    public String getInviteuserid() {
        return inviteuserid;
    }

    public void setInviteuserid(String inviteuserid) {
        this.inviteuserid = inviteuserid == null ? null : inviteuserid.trim();
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel == null ? null : channel.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait == null ? null : portrait.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getSubscribescene() {
        return subscribescene;
    }

    public void setSubscribescene(String subscribescene) {
        this.subscribescene = subscribescene == null ? null : subscribescene.trim();
    }

    public String getIsfollow() {
        return isfollow;
    }

    public void setIsfollow(String isfollow) {
        this.isfollow = isfollow == null ? null : isfollow.trim();
    }

    public Date getFollowtime() {
        return followtime;
    }

    public void setFollowtime(Date followtime) {
        this.followtime = followtime;
    }

    public Date getUnfollowtime() {
        return unfollowtime;
    }

    public void setUnfollowtime(Date unfollowtime) {
        this.unfollowtime = unfollowtime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getXcxopenid() {
        return xcxopenid;
    }

    public void setXcxopenid(String xcxopenid) {
        this.xcxopenid = xcxopenid == null ? null : xcxopenid.trim();
    }

    public Integer getEffectnum() {
        return effectnum;
    }

    public void setEffectnum(Integer effectnum) {
        this.effectnum = effectnum;
    }

    public Integer getInvitecommentnum() {
        return invitecommentnum;
    }

    public void setInvitecommentnum(Integer invitecommentnum) {
        this.invitecommentnum = invitecommentnum;
    }

    public Integer getWkcoursenum() {
        return wkcoursenum;
    }

    public void setWkcoursenum(Integer wkcoursenum) {
        this.wkcoursenum = wkcoursenum;
    }

    public Integer getWkstudyduration() {
        return wkstudyduration;
    }

    public void setWkstudyduration(Integer wkstudyduration) {
        this.wkstudyduration = wkstudyduration;
    }

    public Integer getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(Integer userlevel) {
        this.userlevel = userlevel;
    }

    public Date getXcxcreatetime() {
        return xcxcreatetime;
    }

    public void setXcxcreatetime(Date xcxcreatetime) {
        this.xcxcreatetime = xcxcreatetime;
    }

    public Integer getTargetnum() {
        return targetnum;
    }

    public void setTargetnum(Integer targetnum) {
        this.targetnum = targetnum;
    }

    public Integer getParitycheck() {
        return paritycheck;
    }

    public void setParitycheck(Integer paritycheck) {
        this.paritycheck = paritycheck;
    }

    public String getTargetMessage() {
        return targetMessage;
    }

    public void setTargetMessage(String targetMessage) {
        this.targetMessage = targetMessage == null ? null : targetMessage.trim();
    }

    public Boolean getTestLevel() {
        return testLevel;
    }

    public void setTestLevel(Boolean testLevel) {
        this.testLevel = testLevel;
    }

    public Boolean getUpgradeLevel() {
        return upgradeLevel;
    }

    public void setUpgradeLevel(Boolean upgradeLevel) {
        this.upgradeLevel = upgradeLevel;
    }

    public Boolean getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(Boolean userAuth) {
        this.userAuth = userAuth;
    }

    public Boolean getPhoneAuth() {
        return phoneAuth;
    }

    public void setPhoneAuth(Boolean phoneAuth) {
        this.phoneAuth = phoneAuth;
    }

    public String getHelpuserid() {
        return helpuserid;
    }

    public void setHelpuserid(String helpuserid) {
        this.helpuserid = helpuserid == null ? null : helpuserid.trim();
    }
}