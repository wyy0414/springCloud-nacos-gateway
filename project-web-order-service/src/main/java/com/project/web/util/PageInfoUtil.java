package com.project.web.util;

import com.github.pagehelper.PageInfo;

/**
 * 分页查询条件工具类
 *
 * @Author: wyy
 * @Date: 2020-04-08 17:05
 */
public class PageInfoUtil {
    // 起始页码
    private static final Integer PAGE_NUM = 1;

    // 每页条数
    private static final Integer PAGE_SIZE = 10;

    /**
     * 初始化页面信息对象
     *
     * @param pageNum  页码
     * @param pageSize 没页条数
     * @return
     */
    public static PageInfo initPageInfo(Integer pageNum, Integer pageSize) {
        PageInfo pageInfo = new PageInfo();
        if (pageNum == null) {
            pageInfo.setPageNum(PAGE_NUM);
        } else {
            pageInfo.setPageNum(pageNum);
        }
        if (pageSize == null) {
            pageInfo.setPageSize(PAGE_SIZE);
        } else {
            pageInfo.setPageSize(pageSize);
        }
        return pageInfo;
    }
}
