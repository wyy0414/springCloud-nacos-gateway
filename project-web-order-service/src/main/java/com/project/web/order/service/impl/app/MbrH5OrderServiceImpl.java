package com.project.web.order.service.impl.app;

import com.project.common.core.utils.id.OrderNoWorker;
import com.project.common.core.utils.weChat.WeChatPayUtil;
import com.project.feign.client.SystemServiceClient;
import com.project.web.course.service.impl.common.BaseServiceImpl;
import com.project.web.order.mapper.app.MbrH5OrderMapper;
import com.project.web.order.model.app.MbrH5Order;
import com.project.web.order.service.app.MbrH5OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 会员H5订单业务实现类
 *
 * @Author: wyy
 * @Date: 2020-04-08 00:20
 */
@Service("mbrH5OrderServiceImpl")
public class MbrH5OrderServiceImpl extends BaseServiceImpl<MbrH5Order, Long> implements MbrH5OrderService {

    /**
     * 日志工具类
     */
    public static final Logger log = LoggerFactory.getLogger(MbrH5OrderServiceImpl.class);

    @Autowired
    private MbrH5OrderMapper mbrH5OrderMapper;

    @Autowired
    private SystemServiceClient systemServiceClient;

    /**
     * 微信公众号支付统一下单业务
     *
     * @param mbrH5Order 会员H5订单对象
     * @param request    请求对象request
     * @return
     */
    @Transactional
    @Override
    public Map<String, Object> handleUnifiedOrder(MbrH5Order mbrH5Order, HttpServletRequest request) {

        // 保存订单数据
        mbrH5Order.setOrderNo(OrderNoWorker.get());
        mbrH5Order.setOrderStatus(MbrH5Order.ORDER_STATUS_ENUM.PRE_PAY.getValue());
        save(mbrH5Order);

        // 微信小程序支付预下单
        Map<String, Object> returnMap = WeChatPayUtil.unifiedOrderOfPublicNumber(mbrH5Order.getOrderNo(), mbrH5Order.getPayRealAmt(),
                mbrH5Order.getXyMpOpenid(),
                "/order/anon/weChatNotify/H5_PRIME_ORDER",
                "直播",
                "",
                request);

        return returnMap;
    }
}
