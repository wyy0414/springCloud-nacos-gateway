package com.project.web.order.service.app;

import com.project.web.course.service.common.BaseService;
import com.project.web.order.model.app.MbrH5Order;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * h5订单
 *
 * @Author: wyy
 * @Date: 2020-04-08 00:19
 */
public interface MbrH5OrderService extends BaseService<MbrH5Order, Long> {

    /**
     * 微信公众号支付统一下单业务
     *
     * @param request    请求对象request
     * @param mbrH5Order 会员H5订单对象
     * @return
     */
    Map<String, Object> handleUnifiedOrder(MbrH5Order mbrH5Order, HttpServletRequest request);
}
