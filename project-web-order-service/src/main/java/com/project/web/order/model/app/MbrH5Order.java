/*
 * MbrH5Order.java
 * Copyright(C) 2020-2099 WYY
 * All rights reserved.
 * -----------------------------------------------
 * 2020-04-08 Created
 */
package com.project.web.order.model.app;

import com.project.web.course.model.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 会员h5订单表
 *
 * @version 1.0 2020-04-08
 */
@ApiModel("会员h5订单表")
public class MbrH5Order extends BaseEntity {

    /**
     * 支付人名称【仅针对H5支付】
     */
    @ApiModelProperty("支付人名称【仅针对H5支付】")
    private String payUserName;

    /**
     * 支付人手机号
     */
    @ApiModelProperty("支付人手机号")
    private String payUserPhone;

    /**
     * 公众号openId
     */
    @ApiModelProperty("公众号openId")
    private String xyMpOpenid;

    /**
     * 订单渠道 1：直播0.1元购
     */
    @ApiModelProperty("订单渠道 1：直播 2:小程序 3:公众号")
    private Integer orderChannel;

    /**
     * 订单渠道枚举
     */
    public enum ORDER_CHANNEL_ENUM {
        ZB_LDY("直播", 1),
        ZB_LDL("小程序", 2),
        MP_LDY("公众号", 3);

        /**
         * 枚举名称
         */
        private String name;

        /**
         * 枚举值
         */
        private Integer value;

        /**
         * 枚举有参构造函数
         *
         * @param value 枚举值
         * @param name  枚举名
         */
        ORDER_CHANNEL_ENUM(String name, Integer value) {
            this.value = value;
            this.name = name;
        }

        /**
         * 根据类型值获取类型名称
         *
         * @param value
         * @return 枚举名称
         */
        public String getName(Integer value) {
            // 判断值是否为空
            if (value == null) {
                return "";
            }

            // 循环取出订单类型名称
            for (ORDER_CHANNEL_ENUM orderChannelEnum : ORDER_CHANNEL_ENUM.values()) {
                if (orderChannelEnum.getValue().equals(value)) {
                    return orderChannelEnum.getName();
                }
            }
            return "";
        }

        public String getName() {
            return name;
        }

        /**
         * 设置枚举名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 获取类型值
         */
        public Integer getValue() {
            return value;
        }

        /**
         * 设置类型值
         */
        public void setValue(Integer value) {
            this.value = value;
        }
    }

    /**
     * 交易单号（三方）
     */
    @ApiModelProperty("交易单号（三方）")
    private String transactionNo;

    /**
     * 订单号
     */
    @ApiModelProperty("订单号")
    private String orderNo;

    /**
     * 订单总金额
     */
    @ApiModelProperty("订单总金额")
    private BigDecimal payTotalAmt;

    /**
     * 实际支付金额
     */
    @ApiModelProperty("实际支付金额")
    private BigDecimal payRealAmt;

    /**
     * 订单状态 1：未支付（已生成预支付ID)  2：已取消（取消订单） 3：支付成功（回调通知成功） 4：支付失败（回调通知失败）
     */
    @ApiModelProperty("订单状态1：未支付（已生成预支付ID)2：已取消（取消订单）3：支付成功（回调通知成功）4：支付失败（回调通知失败）")
    private Integer orderStatus;

    public enum ORDER_STATUS_ENUM {
        PRE_PAY(1, "未支付"),
        HAVE_PAY(2, "已取消"),
        SUCCESS_PAY(3, "支付成功"),
        FAIL_PAY(4, "支付失败");

        /**
         * 枚举值
         */
        private Integer value;

        /**
         * 枚举名称
         */
        private String name;

        /**
         * 枚举有参构造函数
         *
         * @param value 枚举值
         * @param name  枚举名
         */
        ORDER_STATUS_ENUM(Integer value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 获取枚举值
         */
        public Integer getValue() {
            return value;
        }

        /**
         * 获取枚举名
         */
        public String getName() {
            return name;
        }
    }

    /**
     * 支付时间
     */
    @ApiModelProperty("支付时间")
    private Date payDate;

    /****************** 扩展属性 ************************/
    /**
     * 销售Id【同sys_user中的user_id】
     */
    @ApiModelProperty("销售Id")
    private String scId;

    /**
     * 渠道
     */
    @ApiModelProperty("渠道")
    private String channel;

    public String getPayUserName() {
        return payUserName;
    }

    public void setPayUserName(String payUserName) {
        this.payUserName = payUserName == null ? null : payUserName.trim();
    }

    public String getPayUserPhone() {
        return payUserPhone;
    }

    public void setPayUserPhone(String payUserPhone) {
        this.payUserPhone = payUserPhone == null ? null : payUserPhone.trim();
    }

    public String getXyMpOpenid() {
        return xyMpOpenid;
    }

    public void setXyMpOpenid(String xyMpOpenid) {
        this.xyMpOpenid = xyMpOpenid == null ? null : xyMpOpenid.trim();
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo == null ? null : transactionNo.trim();
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public BigDecimal getPayTotalAmt() {
        return payTotalAmt;
    }

    public void setPayTotalAmt(BigDecimal payTotalAmt) {
        this.payTotalAmt = payTotalAmt;
    }

    public BigDecimal getPayRealAmt() {
        return payRealAmt;
    }

    public void setPayRealAmt(BigDecimal payRealAmt) {
        this.payRealAmt = payRealAmt;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getScId() {
        return scId;
    }

    public void setScId(String scId) {
        this.scId = scId;
    }


    public static void main(String[] args) {
        System.out.println(ORDER_CHANNEL_ENUM.ZB_LDY.getName(1));
    }
}