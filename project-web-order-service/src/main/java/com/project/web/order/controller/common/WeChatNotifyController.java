package com.project.web.order.controller.common;

import com.project.common.core.config.prop.PropAttributes;
import com.project.common.core.config.prop.PropConfig;
import com.project.common.core.utils.DateUtil;
import com.project.common.core.utils.weChat.WeChatSignUtil;
import com.project.common.core.utils.wx.ResMessageUtil;
import com.project.common.core.utils.wx.WXPayXmlUtil;
import com.project.common.core.utils.wx.message.res.PaymentResult;
import com.project.web.course.controller.common.BaseController;
import com.project.web.order.service.app.MbrH5OrderService;
import com.project.web.util.ORDER_TYPE_ENUM;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * 微信支付异步回调
 *
 * @author wyy
 * @date 2020-04-07
 */
@ApiIgnore
@RestController("orderServiceNotifyController")
@RequestMapping("/order/anon/weChatNotify")
public class WeChatNotifyController extends BaseController {

    @Resource(name = "mbrH5OrderServiceImpl")
    private MbrH5OrderService mbrH5OrderService;

    /**
     * 微信公众号异步通知
     *
     * @param orderTypeEnum 订单类型
     * @param reqXml        异步通知XML数据
     * @return
     */
    @ResponseBody
    @RequestMapping("/{orderTypeEnum}")
    public String asyncNotify(@PathVariable ORDER_TYPE_ENUM orderTypeEnum, @RequestBody String reqXml) throws Exception {
        log.info("\r\n ***************** 异步通知xmlToMap:{}", reqXml);
        // 将通知的结果转换成map
        Map<String, String> xmlMap = WXPayXmlUtil.xmlToMap(reqXml);
        Map<String, String> map = new TreeMap<>();
        for (String key : xmlMap.keySet()) {
            map.put(key, xmlMap.get(key));
        }

        // 判断异步通知return_code
        String returnCode = map.get("return_code");
        if (StringUtils.isEmpty(returnCode) || !StringUtils.equalsIgnoreCase("success", returnCode)) {
            return ResMessageUtil.paymentResultToXml(new PaymentResult("SUCCESS", "OK"));
        }

        // 判断异步通知result_code
        String resultCode = map.get("result_code");
        if (StringUtils.isNotEmpty(resultCode) && StringUtils.equalsIgnoreCase("success", resultCode)) {
            // 根据微信提交的参数获取签名
            String signByKey = WeChatSignUtil.generateMd5SignByKey(map, PropConfig.getProperty(PropAttributes.THIRDPARTY_WX_KEY), "sign");
            log.info("\r\n ********************** 生成签名【{}】**********************", signByKey);

            //  验证签名
            if (!StringUtils.equalsIgnoreCase(map.get("sign"), signByKey)) {
                log.info("\r\n ******************* 签名失败 ******************* \r\n");
                return ResMessageUtil.paymentResultToXml(new PaymentResult("FAIL", "签名失败"));
            }

            // 进入不同类型支付订单处理
            try {
                switch (orderTypeEnum) {
                    case H5_PRIME_ORDER: {// 商品订单
                        String time_end = map.get("time_end");
                        String attach = map.get("attach");
                        Date payDate = DateUtil.convertStrToDate(time_end, "yyyyMMddHHmmss");
                        log.info("\r\n ***************attach参数：{}", attach);
//                        mbrH5OrderService.handleNotify(map.get("out_trade_no"), map.get("transaction_id"), payDate, attach);
                        break;
                    }
                    default: {
                        return ResMessageUtil.paymentResultToXml(new PaymentResult("FAIL", "不存在的交易类型"));
                    }
                }
                log.info("订单回调处理成功,向微信返回成功状态");
                return ResMessageUtil.paymentResultToXml(new PaymentResult("SUCCESS", "OK"));
            } catch (Exception e) {
                log.error("进入catch，业务处理失败:{}", ExceptionUtils.getFullStackTrace(e));
                return ResMessageUtil.paymentResultToXml(new PaymentResult("FAIL", "业务处理失败"));
            }
        }
        return ResMessageUtil.paymentResultToXml(new PaymentResult("FAIL", "业务处理失败"));
    }
}
