package com.project.web.order.controller.app;

import com.github.pagehelper.PageInfo;
import com.project.common.core.utils.exception.CustomException;
import com.project.common.core.utils.exception.RESP_CODE_ENUM;
import com.project.common.core.utils.exception.Result;
import com.project.feign.client.SystemServiceClient;
import com.project.web.course.controller.common.BaseController;
import com.project.web.order.model.app.MbrH5Order;
import com.project.web.order.service.app.MbrH5OrderService;
import com.project.web.util.PageInfoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 会员h5订单控制器
 *
 * @Author: wyy
 * @Date: 2020-04-08 00:24
 */
@Api(tags = "会员h5订单API")
@RestController("orderServiceMbrH5OrderController")
@RequestMapping("/order/mbrH5Order")
public class MbrH5OrderController extends BaseController {

    @Resource(name = "mbrH5OrderServiceImpl")
    private MbrH5OrderService mbrH5OrderService;

    @Autowired
    private SystemServiceClient systemServiceClient;


    /**
     * 会员H5订单统一下单
     *
     * @param mbrH5Order 会员H5订单
     * @return
     */
    @ApiOperation(value = "会员H5订单统一下单")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mbrH5Order", value = "会员H5订单", dataType = "MbrH5Order", required = true)
    })
    @PostMapping(value = "/unifiedOrder")
    public Result<Map<String, Object>> unifiedOrder(MbrH5Order mbrH5Order, HttpServletRequest request) {
        // 判断参数
        if (mbrH5Order == null) {
            throw new CustomException(RESP_CODE_ENUM.ILLEGAL_PARAMETER);
        }

        // 校验必要参数
        String xyMpOpenid = mbrH5Order.getXyMpOpenid();
        String payUserPhone = mbrH5Order.getPayUserPhone();
        BigDecimal payRealAmt = mbrH5Order.getPayRealAmt();
        BigDecimal payTotalAmt = mbrH5Order.getPayTotalAmt();
        String scId = mbrH5Order.getScId();

        if (StringUtils.isBlank(xyMpOpenid) || StringUtils.isBlank(payUserPhone) || payRealAmt == null
                || payTotalAmt == null || StringUtils.isBlank(scId)) {
            throw new CustomException(RESP_CODE_ENUM.MISS_PARAMETER);
        }

        // 公众号支付统一下单
        Map<String, Object> returnMap = mbrH5OrderService.handleUnifiedOrder(mbrH5Order, request);

        return getResult(returnMap);
    }

    /**
     * 查询订单集合【含分页】
     *
     * @param mbrH5Order 会员H5订单
     * @param pageNum    页码
     * @param pageSize   每页条数
     * @return
     */
    @ApiOperation(value = "查询分页订单集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mbrH5Order", value = "会员H5订单", dataType = "MbrH5Order", required = true),
            @ApiImplicitParam(paramType = "query", name = "pageNum", value = "页码", dataType = "Integer", required = true),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页条数", dataType = "Integer", required = true)
    })
    @PostMapping(value = "/getPageList")
    public Result<List<MbrH5Order>> getPageList(MbrH5Order mbrH5Order, Integer pageNum, Integer pageSize) {
        PageInfo pageInfo = PageInfoUtil.initPageInfo(pageNum, pageSize);
        PageInfo<MbrH5Order> mbrH5OrderPageInfo = mbrH5OrderService.selectList(mbrH5Order, pageInfo);
        return getResult(mbrH5OrderPageInfo.getList());
    }
}
