/*
 * MbrH5OrderMapper.java
 * Copyright(C) 2020-2099 北京畅发科技
 * All rights reserved.
 * -----------------------------------------------
 * 2020-04-08 Created
 */
package com.project.web.order.mapper.app;

import com.project.web.course.mapper.common.BaseMapper;
import com.project.web.order.model.app.MbrH5Order;

public interface MbrH5OrderMapper extends BaseMapper<MbrH5Order, Long> {

    /**
     * 根据订单号查询订单
     *
     * @param orderNo 订单号
     * @return
     */
    MbrH5Order getByOrderNo(String orderNo);
}