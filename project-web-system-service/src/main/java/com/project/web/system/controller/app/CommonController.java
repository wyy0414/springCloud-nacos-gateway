package com.project.web.system.controller.app;

import com.project.common.core.utils.exception.CustomException;
import com.project.common.core.utils.exception.RESP_CODE_ENUM;
import com.project.common.core.utils.exception.Result;
import com.project.common.core.utils.redis.RedisClient;
import com.project.web.system.controller.common.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统服务公共API
 */
@Api(tags = "系统服务公共API")
@RestController("systemServiceCommonController")
@RequestMapping("/system/common")
public class CommonController extends BaseController {
    private static final long serialVersionUID = -7624305021416504596L;

    @Resource(name = "redisClient")
    private RedisClient redisClient;

    /**
     * 获取接口令牌
     *
     * @param apiId     api标识
     * @param apiSecret api密钥
     * @return
     */
    @ApiOperation(value = "获取API令牌", notes = "获取API令牌", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apiId", value = "api标识", dataType = "Long"),
            @ApiImplicitParam(name = "apiSecret", value = "api密钥", dataType = "String"),
    })
    @GetMapping(value = "/getApiToken")
    public Result<Map<String, Object>> getApiToken(Long apiId, String apiSecret) {
        // 校验参数
        if (apiId == null || StringUtils.isBlank(apiSecret)) {
            throw new CustomException(RESP_CODE_ENUM.MISS_PARAMETER);
        }

//        // 查询api令牌
//        ApiToken apiToken = new ApiToken();
//        apiToken.setApiId(apiId);
//        List<ApiToken> apiTokens = apiTokenService.selectList(apiToken);
//        if (CollectionUtils.isEmpty(apiTokens)) {
//            throw new CustomException(RESP_CODE_ENUM.REQUIRED_IDENTIFY_NOT_EXIST);
//        }
//
//        // 判断密钥是否正确
//        String apiSecretMd5 = DigestUtils.md5Hex(apiTokens.get(0).getApiSecret());
//        if (!StringUtils.equals(apiSecretMd5, apiSecret)) {
//            throw new CustomException(RESP_CODE_ENUM.PWD_ERROR);
//        }
//
//        // 放入redis
//        String apiTokenStr = RandomStringUtils.randomNumeric(10);
//        redisClient.setAndExpire(RedisConsts.API_TOKEN_KEY + apiId, apiTokenStr, RedisConsts.API_TOKEN_EXPIRE);

        // 初始化返回参数
        Map<String, Object> resultMap = new HashMap();
//        resultMap.put("apiToken", apiTokenStr);
        resultMap.put("apiId", apiId);
        return getResult(resultMap);
    }

    /**
     * 用户登录
     *
     * @param sysUser 登录用户名称
     * @return
     */
//    @ApiOperation(value = "用户登录")
//    @PostMapping(value = "/login")
//    public Result<SysUser> login(@RequestBody SysUser sysUser) {
//
//        String userName = sysUser.getUsername();
//        String loginPwd = sysUser.getLoginPwd();
//        // 参数校验
//        if (StringUtils.isBlank(userName) || StringUtils.isBlank(loginPwd)) {
//            throw new CustomException(RESP_CODE_ENUM.MISS_PARAMETER);
//        }
//
//        // 校验用户是否纯在
//        SysUser sysUserDb = sysUserService.getByUserName(userName);
//        if (sysUserDb == null) {
//            throw new CustomException(RESP_CODE_ENUM.ACCT_NOT_EXIST);
//        }
//
//        // 校验密码
//        String pwdDb = sysUserDb.getLoginPwd();
//        String pwdMd5 = DigestUtils.md5Hex(loginPwd);
//        if (!StringUtils.equalsIgnoreCase(pwdDb, pwdMd5)) {
//            throw new CustomException(RESP_CODE_ENUM.PWD_ERROR);
//        }
//
//        return getResult(sysUserDb);
//    }

}
