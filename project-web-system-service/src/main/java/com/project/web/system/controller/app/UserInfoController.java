package com.project.web.system.controller.app;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.github.pagehelper.PageInfo;
import com.project.common.core.utils.exception.Result;
import com.project.feign.vo.UserInfoVO;
import com.project.web.system.controller.common.BaseController;
import com.project.web.system.model.app.UserInfo;
import com.project.web.system.service.app.UserInfoService;
import com.project.web.util.PageInfoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 学员管理API
 *
 * @Author: wyy
 * @Date: 2020-04-11 17:27
 */
@Api(tags = "学员管理API")
@RestController("systemUserInfoController")
@RequestMapping("/system/auth/userInfo")
public class UserInfoController extends BaseController {

    // 日志工具类
    public static final Logger log = LoggerFactory.getLogger(UserInfoController.class);

    @Resource(name = "userInfoServiceImpl")
    private UserInfoService userInfoService;

    /**
     * 查询系统用户
     *
     * @param userInfo 系统用户
     * @return
     */
    @ApiOperation(value = "查询系统用户列表【含分页】")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "pageNum", value = "页码", dataType = "Integer", required = true),
            @ApiImplicitParam(paramType = "query", name = "pageSize", value = "每页条数", dataType = "Integer", required = true),
            @ApiImplicitParam(paramType = "query", name = "userInfo", value = "学员用户", dataType = "UserInfo", required = true)
    })
    @GetMapping(value = "/getPageList")
    public Result<PageInfo<UserInfo>> getPageList(UserInfo userInfo, Integer pageNum, Integer pageSize) {
        PageInfo pageInfo = PageInfoUtil.initPageInfo(pageNum, pageSize);
        PageInfo<UserInfo> mbrH5OrderPageInfo = userInfoService.selectList(userInfo, pageInfo);
        return getResult(mbrH5OrderPageInfo);
    }

    /**
     * 根据用户ID查询用户
     *
     * @param userId 系统用户
     * @return
     */
    @ApiOperation(value = "根据用户ID查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID", dataType = "String", required = true)
    })
    @GetMapping(value = "/getByUserId")
    public Result<UserInfoVO> getByUserId(String userId) {
        UserInfo userInfo = userInfoService.getById(userId);
        UserInfoVO userInfoVO = new UserInfoVO();
        BeanUtils.copyProperties(userInfo,userInfoVO);
        return getResult(userInfoVO);
    }
}
