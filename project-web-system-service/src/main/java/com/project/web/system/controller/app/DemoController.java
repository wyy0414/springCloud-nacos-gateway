package com.project.web.system.controller.app;

import com.project.common.core.config.prop.PropAttributes;
import com.project.common.core.config.prop.PropConfig;
import com.project.common.core.utils.exception.CustomException;
import com.project.common.core.utils.exception.RESP_CODE_ENUM;
import com.project.common.core.utils.exception.Result;
import com.project.common.core.utils.file.FilePathConsts;
import com.project.common.core.utils.file.FileUtil;
import com.project.common.core.utils.redis.RedisClient;
import com.project.common.core.utils.weChat.WeChatMiniUtil;
import com.project.common.core.utils.weChat.WeChatPayUtil;
import com.project.feign.client.OrderServiceClient;
import com.project.feign.client.SystemServiceClient;
import com.project.web.system.controller.common.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 控制器编码范例
 *
 * @author wyy
 * @date 2019-08-20 19:57
 */
@Api(tags = "编码范例")
@RestController("SystemServiceDemoController")
@RequestMapping("/system/auth/demo")
public class DemoController extends BaseController {

    @Resource(name = "redisClient")
    private RedisClient redisClient;

    @Autowired
    private OrderServiceClient orderServiceClient;

    /**
     * 测试接口
     *
     * @param test 测试参数
     * @return
     */
    @ApiOperation(value = "测试接口")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "test", value = "测试参数", dataType = "String", required = true)
    })
    @GetMapping(value = "/test")
    public Result<String> test(String test) {
        System.out.println(test);
        orderServiceClient.test(test);
        return getResult(test);
    }

    /**
     * 普通控制器接口书写规范
     *
     * @return
     */
    @ApiOperation(value = "普通接口书写范例")
    @RequestMapping(value = "/updateSysConfig", method = RequestMethod.GET)
    public Result<Map<String, Object>> updateSysConfig() {

        // 属性文件中取值
        PropConfig.getProperty(PropAttributes.SYSTEM_SERVICE_DATAID);

        // redis存取值
        redisClient.set("key", 123);
        redisClient.get("key");

        // controller层异常方式
        if (true) {
            // 必要的地方打印日志【日志使用占位符方式，如果打印堆栈信息，可以使用ExceptionUtils】
            log.info("此处有错误:{}", "错误信息");

            // 如果需要返回错误提示【框架中错误返回均以全局异常处理】
            throw new CustomException(RESP_CODE_ENUM.ILLEGAL_PARAMETER);
        }

        // 如果返回对象信息，可以为简单类型、复合类型[Object、Map、List、model实体等]
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("name", "小栗子");
        returnMap.put("集合", new ArrayList<String>());
        returnMap.put("数字", 1231312L);
        return getResult(returnMap);
    }

    /**
     * 文件上传接口书写规范
     *
     * @param testFile 上传文件
     * @return
     */
    @ApiOperation(value = "文件上传范例")
    @ApiImplicitParams(
            @ApiImplicitParam(paramType = "query", name = "uploadFile", value = "上传文件", required = true, dataType = "MultipartFile"))
    @PostMapping(value = "/uploadFile")
    public Result<String> uploadFile(MultipartFile testFile) {
        // 1、图片异步上传返回图片URL，此时图片保存在临时文件夹,
        String orgFileName = FileUtil.getNFSFileName(testFile);

        // 2、表单提交，需将临时文件夹中文件拷贝到图片仓库文件夹中，并返回图片URL
        String fileUrl = FileUtil.copyNFSByFileName(orgFileName, FilePathConsts.FILE_MEDIA_PATH);

        // 3、如果是编辑页面，需要先删除原图片
        FileUtil.deleteNFSByFileUrl(fileUrl);

        // 4、如果是直接图片上传到NFS服务器，返回文件NFS访问URL
        String nfsUrl = FileUtil.getNFSUrl(testFile, FilePathConsts.FILE_MEDIA_PATH);

        return getResult(nfsUrl);
    }

    /**
     * 测试微信支付统一下单
     */
    @ApiOperation(value = "测试微信支付统一下单")
    @PostMapping(value = "/unifiedOrder")
    public Result<Map<String, Object>> unifiedOrderOfWxMini(HttpServletRequest request) {
        // 微信支付预下单
        Map<String, Object> returnMap = WeChatPayUtil.unifiedOrderOfWxMini("123098",
                new BigDecimal("0.01"), "o1tUJ42W_-5d0DTBB-lI-S7ukgow",
                "/paymentNotify/async_notify/MBR_LEVEL_ORDER.jhtml", "测试订单", request);

        return getResult(returnMap);
    }

    /**
     * 测试生成永久小程序二维码
     */
    @ApiOperation(value = "测试生成永久小程序二维码")
    @PostMapping(value = "/wxMiniQrCode")
    public Result<String> wxMiniQrCode() {
        // 返回小程序二维码图片服务器地址【主要用于：景点签到，会员邀请】
        String miniQrCodeImg = WeChatMiniUtil.getMiniQrCodeImg("123123", "", 600, null, false);

        return getResult(miniQrCodeImg);
    }
}
