package com.project.web.system.service.common;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * service 基础接口
 */
public interface BaseService<T, ID extends Serializable> extends Serializable {

    /**
     * 根据主键删除对象
     *
     * @param id 主键
     * @return
     */
    int delete(ID id);

    /**
     * 保存对象
     *
     * @param entity 对象
     * @return
     */
    T save(T entity);

    /**
     * 根据主键查询对象
     *
     * @param id 主键
     * @return
     */
    T getById(ID id);

    /**
     * 跟新对象
     *
     * @param entity
     * @return
     */
    T update(T entity);

    /**
     * 查询对象集合
     *
     * @param entity 对象
     * @param pageInfo 分页对象
     * @return
     */
    PageInfo<T> selectList(T entity, PageInfo<T> pageInfo);

    /**
     * 查询对象集合
     * @param entity 对象
     * @return
     */
    List<T> selectList(T entity);
}
