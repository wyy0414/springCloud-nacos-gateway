package com.project.web.system.mapper.common;

import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * mapper父接口
 */
public interface BaseMapper<T, ID extends Serializable> extends Serializable {

    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    int delete(@Param("id") ID id);

    /**
     * 保存对象
     *
     * @param entity
     * @return
     */
    int save(T entity);

    /**
     * 更新对象
     *
     * @param entity
     * @return
     */
    int update(T entity);

    /**
     * 根据ID查询对象
     *
     * @param id 主键ID
     * @return
     */
    T getById(@Param("id") ID id);

    /**
     * 动态查询
     *
     * @param entity 实体对象
     * @return
     */
    List<T> selectList(T entity);
}
