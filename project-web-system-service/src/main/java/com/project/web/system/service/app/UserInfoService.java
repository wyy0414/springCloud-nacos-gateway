package com.project.web.system.service.app;

import com.github.pagehelper.PageInfo;
import com.project.web.system.model.app.UserInfo;

import java.util.List;

/**
 * @Author: wyy
 * @Date: 2020-04-09 02:43
 */
public interface UserInfoService {

    /**
     * 根据主键删除对象
     *
     * @param userId 主键
     * @return
     */
    int delete(String userId);

    /**
     * 保存对象
     *
     * @param userInfo 对象
     * @return
     */
    UserInfo save(UserInfo userInfo);

    /**
     * 根据主键查询对象
     *
     * @param userId 主键
     * @return
     */
    UserInfo getById(String userId);

    /**
     * 跟新对象
     *
     * @param userInfo
     * @return
     */
    UserInfo update(UserInfo userInfo);

    /**
     * 查询对象集合
     *
     * @param userInfo 对象
     * @param pageInfo 分页对象
     * @return
     */
    PageInfo<UserInfo> selectList(UserInfo userInfo, PageInfo<UserInfo> pageInfo);

    /**
     * 查询对象集合
     *
     * @param userInfo 对象
     * @return
     */
    List<UserInfo> selectList(UserInfo userInfo);
}
