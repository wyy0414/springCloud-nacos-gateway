package com.project.web.system.service.impl.app;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.project.web.system.mapper.app.UserInfoMapper;
import com.project.web.system.model.app.UserInfo;
import com.project.web.system.service.app.UserInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wyy
 * @Date: 2020-04-09 02:48
 */
@Service("userInfoServiceImpl")
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    protected UserInfoMapper userInfoMapper;

    /**
     * @param userId 主键
     * @return
     */
    @Override
    public int delete(String userId) {
        if (StringUtils.isBlank(userId)) {
            return 0;
        }
        return userInfoMapper.delete(userId);
    }

    /**
     * @param userInfo 对象
     * @return
     */
    @Override
    public UserInfo save(UserInfo userInfo) {
        userInfoMapper.save(userInfo);
        return userInfo;
    }

    /**
     * @param userId 主键
     * @return
     */
    @Override
    public UserInfo getById(String userId) {

        if (StringUtils.isBlank(userId)) {
            return null;
        }
        return userInfoMapper.getById(userId);
    }

    /**
     * @param userInfo
     * @return
     */
    @Override
    public UserInfo update(UserInfo userInfo) {
        userInfoMapper.update(userInfo);
        return userInfo;
    }

    /**
     * @param userInfo 对象
     * @param pageInfo 分页对象
     * @return
     */
    @Override
    public PageInfo<UserInfo> selectList(UserInfo userInfo, PageInfo<UserInfo> pageInfo) {
        if (pageInfo != null) {
            PageHelper.startPage(pageInfo.getPageNum(), pageInfo.getPageSize());
        }
        return new PageInfo(userInfoMapper.selectList(userInfo));
    }

    /**
     * @param userInfo 对象
     * @return
     */
    @Override
    public List<UserInfo> selectList(UserInfo userInfo) {
        return userInfoMapper.selectList(userInfo);
    }
}
