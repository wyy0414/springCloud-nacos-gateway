/*
 * UserInfoMapper.java
 * Copyright(C) 2020-2099 WYY
 * All rights reserved.
 * -----------------------------------------------
 * 2020-04-09 Created
 */
package com.project.web.system.mapper.app;

import com.project.web.system.model.app.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserInfoMapper {

    /**
     * 根据ID删除
     *
     * @param userId
     * @return
     */
    int delete(@Param("userid") String userId);

    /**
     * 保存对象
     *
     * @param userInfo
     * @return
     */
    int save(UserInfo userInfo);

    /**
     * 更新对象
     *
     * @param userInfo
     * @return
     */
    int update(UserInfo userInfo);

    /**
     * 根据ID查询对象
     *
     * @param userid 主键ID
     * @return
     */
    UserInfo getById(@Param("userid") String userid);

    /**
     * 动态查询
     *
     * @param userInfo 实体对象
     * @return
     */
    List<UserInfo> selectList(UserInfo userInfo);
}