
package com.project.web.config.swagger;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * SwaggerBootstrapUI API
 *
 * @author wyy
 * @date 2021-03-04
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

    @Bean(value = "systemApi")
    @Order(value = 1)
    public Docket groupRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(groupApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.project.web.system.controller"))
                .paths(PathSelectors.any())
                .build().securityContexts(Lists.newArrayList(securityContext(), securityContext1())).securitySchemes(Lists.<SecurityScheme>newArrayList(apiTokenKey(), apiIdKey()));
    }

    private ApiInfo groupApiInfo() {
        return new ApiInfoBuilder()
                .title("System Service Restful API")
                .description("一、接口调用规则如下" +
                        "\r\n 1: 接口域名：https://aip.jianxc.com" +
                        "\r\n 2: /chatMate/auth/* header中参数为必填" +
                        "\r\n 3: /chatMate/anon/* header中参数为非必填" +
                        "\r\n 4: /chatMate/common/* header中参数为非必填" +
                        "\r\n 二、参数传递类型如下" +
                        "\r\n 1:【header】参数在request headers 里边提交" +
                        "\r\n 2:【form】以表单的形式提交仅POST方式" +
                        "\r\n 3:【query】get方式,参数完成自动映射赋值" +
                        "\r\n 4:【path】URL地址路径作为参数" +
                        "\r\n 5:【body】支持POST方式JSON提交参数")
                .termsOfServiceUrl("https://api.jianxc.com")
                .contact(new Contact("北京坚小持文化科技有限公司", "http://www.jianxc.com", "system@jianxc.com"))
                .version("1.0.0")
                .build();
    }


    private ApiKey apiTokenKey() {
        return new ApiKey("apiToken", "ApiToken", "header");
    }

    private ApiKey apiIdKey() {
        return new ApiKey("apiId", "ApiId", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }

    private SecurityContext securityContext1() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth1())
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(new SecurityReference("apiToken", authorizationScopes));
    }

    List<SecurityReference> defaultAuth1() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(new SecurityReference("apiId", authorizationScopes));
    }

}
