# springCloud-greenwich-SR5 版本

#### 介绍
   springCloud、Nacos服务注册发现、Gateway网关、swagger-bootstrap-ui聚合Swagger文档

#### 安装教程

1. 项目所用数据库文件可以私信
2. 服务拆分仅为了模仿多服务通信【见谅】

#### 框架版本说明

1. SpringBoot2.3.9.RELEASE
2. SpringCloud Hoxton.SR9
3. SpringCloud-Alibaba-2.2.5.RELEASE
4. SpringCLoud-gateway
5. SpringCloud-ALibab-Nacos
7. SpringCloud-openFeign服务调用
8. swagger-bootstrap-ui-1.9.6


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)